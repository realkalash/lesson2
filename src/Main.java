public class Main {

    public static void main(String[] args) {
        // Формула ax^2 + bx + c = 0
        int a = 3;
        int b = 4;
        int c = 5;
        int discriminant;
        double root1, root2;

        discriminant = (b * b) - 4 * a * c;

        if (discriminant < 0) {
            System.out.println("Корней нет");

        } else if (discriminant == 0) {
            root1 = ((-b + Math.sqrt(discriminant)) / (2 * a));
            System.out.println("Уровнение имеет 1 корень: " + root1);
        }

        else if (discriminant>0) {
            root1 = ((-b + Math.sqrt(discriminant)) / (2 * a));
            root2 = ((-b - Math.sqrt(discriminant)) / (2 * a));

            System.out.println("Уровнение имеет 2 кореня: " + root1 + " и " + root2);
        }
    }
}
